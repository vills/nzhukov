<?php

/**
 * Minus operation
 * 
 * @author Nikolay Zhukov, njhuckov@gmail.com
 * @
 * Description of Minus
 *
 * 
 */

include_once 'IAction.php';

class Minus implements IAction {

    /**
     * Количество операндов, реализующих операцию в классе
     * 
     * @var integer
     */
    private $totaloperands = 2;

    function __construct() {
        
    }

    public function GetNumbOfArgsInteger() {
        return $this->totaloperands;
    }

    /**
     * Получить знак операции
     * 
     * @return string
     */
    public function GetActionString() {
        return "-";
    }

    /**
     * Вычислить операцию минус
     * 
     * @param array $args массив операндов
     * 
     * @return integer
     */
    public function Evaluate($args) {
        if (!is_array($args))
            throw new InvalidArgumentException("Array not provided in argument!");
        if (count($args) != $this->totaloperands)
            throw new InvalidArgumentException("Wrong number of arguments for this action!");
        return $args[0] - $args[1];
    }

}

?>
