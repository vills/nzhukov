<?php
// Определяем переменную ActionInterfaceName, обозначающую имя интерфейса,
// который будем использовать
define("ActionInterfaceName", "IAction");

$operationsAr = Array();
$optionsAr = Array();
// Подгрузка классов, отвечающих за реализацию операций
$dir = opendir("."); # Open the path
while ($file = readdir($dir)) {

    if (preg_match("/.php/", $file)) { # Look at only files with a .php extension   
        $fileContent = file_get_contents($file); //считываем как текст код класса с операцией

        preg_match_all("|class(\s)+(\w+)|", $fileContent, $out, PREG_PATTERN_ORDER);
        //проверяем что это действительно класс

        $className = $out[2][0]; //берем имя класса из результата регулярного поиска
        if ($className != "")
            $operationsAr[] = $className;
    }
}
for ($i = 0; $i < count($operationsAr); $i++) {
    $fullPathToFile = dirname(__FILE__) . "/" . $operationsAr[$i] . ".php";

    include $fullPathToFile;
    $opReflectedObject = new ReflectionClass($operationsAr[$i]);
    $rcIAction = new ReflectionClass(ActionInterfaceName);
    $rc = $opReflectedObject->implementsInterface($rcIAction);

    if (!$rc)
        throw new Exception("Ошибка с реализацией класса " . $operationsAr . ". Операция недоступна для выполнения.");
    else {
        $operationNewObj = $opReflectedObject->newInstance();
        if (!isset($operationNewObj))
            throw new Exception("Объект класса " . $operationNewObj . " не был создан.");
        else {
            $optionsAr[$operationNewObj->GetActionString()] = $operationsAr[$i];
        }
    }
}
?>
<!DOCTYPE html>
<html lang="ru">
    <head>
        <meta charset="utf-8" />
        <title> Farslab | OMG, It's calc. Again</title>
        <script type="text/javascript" src="//code.jquery.com/jquery.min.js"></script> 
    </head>
    <body>
        <?php
        if (($_GET["do"] == "act") && ($_GET["o"] != "0")) {

            //foreach($)
            /*
              include dirname(__FILE__) . "/Minus.php";  // там будет className
              //считаем файлы, с помощью регулярок и phpDoc заполним селект
              //для выбора операций. далее будет типа selectedValue

              $reflectedObject = new ReflectionClass("Minus");  //ReflectionObject != ReflectionClass
              $rcIAction = new ReflectionClass(ActionInterfaceName); //чтобы иметь возможность проверить
              //реализует ли считанный нами класс интерфейс IAction
              print_r($reflectedObject);

              $rc = $reflectedObject->implementsInterface($rcIAction);
              ;
              if (!$rc) {
              echo "Implementation failure!";
              exit();
              }

              //если не реализован интерфейс, не делать больше ничего
              // work
              $tempName = "Plus";
              //$plus = new $tempName;  //имя из селекта
              $plus = $reflectedObject->newInstance("");

              print_r($plus);

              if (!isset($plus))
              return; //exception nothing happened

              function func($action) {
              echo $action->GetActionString();
              }

              func($plus);

              echo "<br/>done<br/>";

              function calc($action, $operands) {
              echo $action->Evaluate($operands);
              }

              $ops = array(2, -2);
              echo calc($plus, $ops);
              echo "<br/>calc successfully processed!<br/>";
             * 
             */
            exit(0);
            foreach ($names as $name) {
                $objs[] = new $name();
            }
            foreach ($objs as $obj) {
                $obj->GetActionString();
            }
        }
        ?>
        <?php
        //Формирование списка операций
        ?> 
        <!-- Вывод формы для выбора действия операций --> 
        <h4>Найдены модули:</h4>
        <ul>
            <?php
            foreach ($optionsAr as $key => $value)
                echo "<li>" . $value . "</li>";
            ?>
        </ul>
        <form action="<?php echo $_SERVER['PHP_SELF']; ?>"> 
            <select id="operation" name="o">
                <option value="0">Выберите операцию</option>
                <?php
                foreach ($optionsAr as $key => $value) {
                    echo "<option value=\"" . $value . "\">" . $key . "</option>";
                }
// Вывод option с загруженными операциями 
                ?>
            </select>
            <input type="hidden" name="do" value="act"><br/>
            <br/>
            <input type="submit" value="Вычислить">&nbsp; &nbsp;<input type="reset" value="Очистить значения">
        </form>


    </body>

</html>
