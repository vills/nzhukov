<?php
/**
 * Description of IAction
 *
 * @author boy
 */
interface IAction {
    
    public function GetActionString();
    
    public function GetNumbOfArgsInteger();
    
    public function Evaluate($args);
}

?>
