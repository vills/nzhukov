<?php
/**
 * Description of Multiply
 *
 * 
 */

include_once 'IAction.php';

class Multiply implements IAction {

    private $totaloperands = 2;

    function __construct() {
        
    }

    public function GetNumbOfArgsInteger() {
        return $this->totaloperands;
    }

    public function GetActionString() {
        return "*";
    }

    public function Evaluate($args) {
        if (!is_array($args))
            throw new InvalidArgumentException("Arrray not provided in argument!");
        if (count($args) != 2)
            throw new InvalidArgumentException("Wrong number of arguments for this action!");
        return $args[0] * $args[1];
    }

}
?>
