<?php

class summa extends calc {
  public static $name = "Сложение";

  function __construct() {
  }

  function getName() {
    return self::$name;
  }

  function performAction() {
    $n = parent::getNumbers();
    return $n[0] + $n[1];
  }
}

?>