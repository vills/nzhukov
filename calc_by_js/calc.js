var calc = {
  actions: [  'summa',
              'raznost'
           ],
  memory: ['','','','',''],
  isNextFreeLabel: false,

  initialize: function() {
    this.createBaseHtml();
    this.loadActions();
  },

  createBaseHtml: function() {
    $('body').append('<div id="calc"><div id="memory" style="height:25px;"></div><input type="text" id="label" style="text-align:right" readonly><br><div id="nums">');
    $.each([1,2,3,4,5,6,7,8,9,0], function(i,v) {
      $('body').append('<input type="button" value="'+v+'" onclick="calc.pressNum(\''+v+'\')" style="width:auto">');
      if (v % 3 === 0)
        $('body').append('<br>');
    });
    $('body').append('<input type="button" value="Clear" onclick="calc.freeMemory()">');
    $('body').append('</div><br><div id="actions"></div><br><input type="button" onclick="calc.calculate()" value="Посчитать"></div>');
  },

  loadActions: function(actions) {
    $.each(this.actions, function(aInd, aVal) {
      $.getJSON('actions/' + aVal + '.json', function(data) {
        $('#actions').append('<input type="button" value="' + data.value + '" onclick="calc.pressAction(\''+data.action+'\')"><br>');
      });
    });
  },

  pressNum: function(num) {
    if (calc.isNextFreeLabel) {
      $("#label").val('');
      calc.isNextFreeLabel = false;
    }
    $("#label").val($("#label").val() + num);
  },

  pressAction: function(act) {
    if (calc.memory[1] === '') {
      calc.memory[0] = $("#label").val();
      calc.memory[1] = act;
      calc.isNextFreeLabel = true;
    }
    calc.drawMemory();
  },

  drawMemory: function() {
    $("#memory").html(calc.memory[0] + ' ' + calc.memory[1] + ' ' + calc.memory[2] + ' ' + calc.memory[3] + ' ' + calc.memory[4]);
  },

  calculate: function() {
    calc.memory[2] = $("#label").val();
    calc.drawMemory();
    if (calc.memory[0] !== '' && calc.memory[2] !== '') {
      $("#label").val('');
      ret = new Function('a', 'return parseInt(a[0])' + calc.memory[1] + 'parseInt(a[2])');
      calc.memory[3] = '=';
      calc.memory[4] = ret(calc.memory);
      calc.drawMemory();
      calc.freeMemory();
      return;
    }
    console.log("Введите второе значение то!");
  },

  freeMemory: function() {
    calc.memory[0] = ''; calc.memory[1] = ''; calc.memory[2] = ''; calc.memory[3] = ''; calc.memory[4] = '';
    $("#label").val('');
  }
};

$(document).ready(function() {
    calc.initialize();
});
