<?php

class calc {
  public static $num = array();
  public static $action = '';

  function __construct($num1=0, $num2=0) {
    $this->scanActions();
    self::$num = array($num1, $num2);
  }

  function getNumbers() {
    return self::$num;
  }

  function scanActions() {
    foreach (glob("/var/www/calc_by_php/actions/*.class.php") as $index => $file) {
      include_once "$file";
      $a = basename($file, '.class.php');
      $this->action[$a] = new $a;
    }
  }
}


if (isset($_POST) && count($_POST)>0) {
  $c = new calc($_POST['num1'], $_POST['num2']);
  $m = array_keys($_POST["action"]);
  $m = $m[0];
  echo "Результат: " . $c->action[$m]->performAction();
}

if (!isset($c) || !is_object($c))
  $c = new calc();

?>

<form action='' method='post'>
  <p>Первое число:
    <input type='text' value='' name='num1'>
  </p>

  <p>Второе число:
    <input type='text' value='' name='num2'>
  </p>

<?php
foreach ($c->action as $key=>$value) {
  echo "<input type='submit' value='" . $c->action[$key]->getName() . "' name='action[" . $key . "]'><br>";
}
?>

</form>